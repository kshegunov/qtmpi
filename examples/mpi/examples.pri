INCLUDEPATH += ../../../include

LIBS += -L$$OUT_PWD/../../../lib

TEMPLATE = app

QT += core mpi

CONFIG += console
CONFIG -= app_bundle

target.path = $$[QT_INSTALL_EXAMPLES]/mpi/$$TARGET
INSTALLS += target
