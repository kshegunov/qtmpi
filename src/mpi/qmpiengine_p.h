/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtMpi API. It exists only
// as an implementation detail. This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#ifndef QMPIENGINE_P_H
#define QMPIENGINE_P_H

#include "qmpiglobal.h"

#include <QTimer>

QT_BEGIN_NAMESPACE

class QMpiEngine;
class QMpiCommunicator;

class Q_MPI_LOCAL QMpiEnginePrivate
{
    Q_DECLARE_PUBLIC(QMpiEngine)

    enum State  {
        InactiveState, InvalidState, RunningState, ShuttingDownState
    };

public:
    QMpiEnginePrivate(int &, char ** &, QMpiEngine *);
    ~QMpiEnginePrivate();

    static QMpiEnginePrivate * instance();

    QMpiEngine * engine();
    QMpiCommunicator * communicator();
    QTimer * timer();

    qint64 maxLatency() const;

    void start();
    void shutdown();

private:
    QMpiEngine * q_ptr;

    qint64 timeout;       // in ms
    QTimer pollTimer;
    State state;
    QMpiCommunicator * globalCommunicator;

    static QMpiEnginePrivate * engineInstance;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QMpiEnginePrivate * QMpiEnginePrivate::instance()
{
    return engineInstance;
}

inline QMpiEngine * QMpiEnginePrivate::engine()
{
    return q_func();
}

inline QMpiCommunicator * QMpiEnginePrivate::communicator()
{
    return globalCommunicator;
}

inline QTimer * QMpiEnginePrivate::timer()
{
    return &pollTimer;
}

inline qint64 QMpiEnginePrivate::maxLatency() const
{
    return timeout;
}

QT_END_NAMESPACE

#endif // QMPIENGINE_P_H
