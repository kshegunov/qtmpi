/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpinode.h"
#include "qmpievent.h"
#include "qmpimessage.h"
#include "qmpirequest.h"
#include "qmpicommunicator.h"

#include "qmpinode_p.h"
#include "qmpimessage_p.h"

QT_BEGIN_NAMESPACE

template class QList<QMpiNode *>;

QMpiNode::QMpiNode(QtMpi::NodeId id, QMpiCommunicator * communicator)
    : QMpiMessageReceiver(*new QMpiNodePrivate(id, communicator, this), communicator)
{
    bind<QMpiAboutToQuitMessage>(this, &QMpiNode::aboutToQuit);
    bind<QMpiQuitMessage>(this, &QMpiNode::quit);
}

QMpiCommunicator * QMpiNode::communicator()
{
    Q_D(QMpiNode);
    return d->communicator;
}

const QMpiCommunicator * QMpiNode::communicator() const
{
    Q_D(const QMpiNode);
    return d->communicator;
}

bool QMpiNode::isRoot() const
{
    Q_D(const QMpiNode);
    return !d->id;
}

bool QMpiNode::isCurrent() const
{
    Q_D(const QMpiNode);
    return d->id == d->communicator->currentNodeId();
}

bool QMpiNode::isActive() const
{
    Q_D(const QMpiNode);
    return d->active;
}

QtMpi::NodeId QMpiNode::id() const
{
    Q_D(const QMpiNode);
    return d->id;
}

QMpiNodeList QMpiNode::peers() const
{
    Q_D(const QMpiNode);
    return d->communicator->peers(this);
}

QMpiRequest * QMpiNode::send(const QMpiMessage & message, QtMpi::CommunicationType communicationType)
{
    Q_D(QMpiNode);
    return d->communicator->send(this, message, communicationType);
}

void QMpiNode::quit()
{
    if (isCurrent())  {
        qApp->quit();
        return;
    }

    send(QMpiQuitMessage());
}

QT_END_NAMESPACE
