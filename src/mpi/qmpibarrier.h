/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPIBARRIER_H
#define QMPIBARRIER_H

#include "qmpiglobal.h"
#include <QObject>

QT_BEGIN_NAMESPACE

class QMpiRequest;

class QMpiBarrierPrivate;
class Q_MPI_EXPORT QMpiBarrier : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(QMpiBarrier)
    Q_DECLARE_PRIVATE(QMpiBarrier)

public:
    enum { WaitForever = -1 };

    explicit QMpiBarrier(QObject * = nullptr);
    ~QMpiBarrier() override;

    void addRequest(QMpiRequest *);
    void removeRequest(QMpiRequest *);

    bool wait(int = WaitForever);
    bool waitAny(int = WaitForever);

    static bool wait(QMpiRequest *, int = WaitForever);

    QMpiBarrier & operator << (QMpiRequest *);

Q_SIGNALS:
    void finished();
    void canceled(QMpiRequest *);
    void error(QMpiRequest *);

private:
    QMpiBarrierPrivate * d_ptr;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QMpiBarrier & QMpiBarrier::operator << (QMpiRequest * request)
{
    addRequest(request);
    return *this;
}

inline bool QMpiBarrier::wait(QMpiRequest * request, int timeout)
{
    QMpiBarrier barrier;
    barrier.addRequest(request);
    return barrier.wait(timeout);
}

QT_END_NAMESPACE

#endif // QMPIBARRIER_H
