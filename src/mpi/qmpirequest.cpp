/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpirequest.h"
#include "qmpirequest_p.h"
#include "qmpiengine_p.h"

#include <QEventLoop>
#include <QTimer>

QT_BEGIN_NAMESPACE

QMpiRequest::QMpiRequest(QObject * parent)
    : QMpiRequest(*new QMpiRequestPrivate(this), parent)

{
}

QMpiRequest::QMpiRequest(QMpiRequestPrivate & d, QObject * parent)
    : QObject(parent), d_ptr(&d)
{
    QObject::connect(this, &QMpiRequest::finished, this, &QMpiRequest::deleteLater);
    QObject::connect(this, &QMpiRequest::canceled, this, &QMpiRequest::deleteLater);
    QObject::connect(this, &QMpiRequest::error, this, &QMpiRequest::deleteLater);
}

QMpiRequest::~QMpiRequest()
{
    delete d_ptr;
}

bool QMpiRequest::wait(int timeout)
{
    enum {
        Ok, Error
    };

    QEventLoop eventLoop;
    auto stopEventLoop = [&eventLoop] () -> void {
        eventLoop.exit(Error);
    };

    QObject::connect(this, &QMpiRequest::finished, &eventLoop, &QEventLoop::quit);
    QObject::connect(this, &QMpiRequest::canceled, &eventLoop, &QEventLoop::quit);
    QObject::connect(this, &QMpiRequest::error, &eventLoop, stopEventLoop);

    if (timeout > 0)  {
        QTimer * timer = new QTimer(&eventLoop);
        timer->setSingleShot(true);
        timer->setInterval(timeout);
        timer->start();

        QObject::connect(timer, &QTimer::timeout, &eventLoop, stopEventLoop);
    }

    return eventLoop.exec() == Ok;
}

void QMpiRequest::cancel()
{
    Q_ASSERT_X(false, "QMpiRequest::cancel", "QtMpi: Request canceling is not implemented!");
}

QT_END_NAMESPACE
