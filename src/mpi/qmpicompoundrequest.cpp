/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpicompoundrequest.h"
#include "qmpicompoundrequest_p.h"

QT_BEGIN_NAMESPACE

QMpiCompoundRequest::QMpiCompoundRequest(QObject * parent)
    : QMpiRequest(*new QMpiCompoundRequestPrivate(this), parent)
{
}

void QMpiCompoundRequest::watch(QMpiRequest * child)
{
    Q_D(QMpiCompoundRequest);
    if (d->subrequests.contains(child))
        return;

    d->subrequests.insert(child);

    QObject::connect(child, &QMpiRequest::canceled, this, &QMpiCompoundRequest::canceled);
    QObject::connect(child, &QMpiRequest::error, this, &QMpiCompoundRequest::error);
    QObject::connect(child, &QMpiRequest::finished, this, std::bind(&QMpiCompoundRequestPrivate::requestFinished, d, child));
    QObject::connect(child, &QObject::destroyed, this, [d] (QObject * object) -> void {
        d->subrequests.remove(reinterpret_cast<QMpiRequest *>(object));
    });
}

void QMpiCompoundRequest::forget(QMpiRequest * request)
{
    Q_D(QMpiCompoundRequest);
    if (d->subrequests.remove(request))
        QObject::disconnect(request, nullptr, this, nullptr);
}

bool QMpiCompoundRequest::isWatching() const
{
    Q_D(const QMpiCompoundRequest);
    return d->subrequests.size();
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

void QMpiCompoundRequestPrivate::requestFinished(QMpiRequest * child)
{
    if (!subrequests.remove(child))
        return;

    Q_Q(QMpiCompoundRequest);
    emit q->finished(child);
    if (subrequests.size() == 0)
        emit q->QMpiRequest::finished();
}

QT_END_NAMESPACE

#include "moc_qmpicompoundrequest.cpp"
