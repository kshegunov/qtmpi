#ifndef QMPILOG_H
#define QMPILOG_H

#include "qmpiglobal.h"

#include <QLoggingCategory>

QT_BEGIN_NAMESPACE

Q_DECLARE_LOGGING_CATEGORY(Mpi)

QT_END_NAMESPACE

#endif // QMPILOG_H
