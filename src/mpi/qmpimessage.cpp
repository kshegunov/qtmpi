/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpimessage.h"
#include "qmpimessage_p.h"

QT_BEGIN_NAMESPACE

QMpiMessage::QMpiMessage()
{
}

QMpiMessage::~QMpiMessage()
{
}

bool QMpiMessage::serialize(QDataStream &) const
{
    return true;
}

bool QMpiMessage::deserialize(QDataStream &)
{
    return true;
}

QMpiAcknowledgeMessage::QMpiAcknowledgeMessage(quint32 id)
    : requestId(id)
{
}

bool QMpiAcknowledgeMessage::serialize(QDataStream & stream) const
{
    stream << requestId;
    return stream.status() == QDataStream::Ok;
}

bool QMpiAcknowledgeMessage::deserialize(QDataStream & stream)
{
    stream >> requestId;
    return stream.status() == QDataStream::Ok;
}

QT_END_NAMESPACE

#include "moc_qmpimessage.cpp"
#include "moc_qmpimessage_p.cpp"
