/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPI_GLOBAL_H
#define QMPI_GLOBAL_H

#include <QtGlobal>
#include <QMetaType>

QT_BEGIN_NAMESPACE

#if !defined(QT_MPI_STATICLIB)
    #if defined(QT_BUILD_MPI_LIB)
        #define Q_MPI_EXPORT Q_DECL_EXPORT
        #define Q_MPI_LOCAL Q_DECL_HIDDEN
    #else
        #define Q_MPI_EXPORT Q_DECL_IMPORT
        #define Q_MPI_LOCAL
    #endif
#else
    #define Q_MPI_EXPORT
    #define Q_MPI_LOCAL
#endif

namespace QtMpi
{
    typedef qint32 NodeId;
    typedef qint32 MessageTag;
    typedef int MessageType;

    enum : NodeId { RootNodeId = 0};
    enum : MessageTag { InvalidMessageTag = -1 };
    enum : MessageType { UnknownMessageType = QMetaType::UnknownType };

    // By default use non-blocking asynchronous message passing
    enum CommunicationTypes {
        AsynchronousCommunication = 0x00, SynchronousCommunication = 0x01, AutoCommunication = QtMpi::AsynchronousCommunication
    };
    Q_DECLARE_FLAGS(CommunicationType, CommunicationTypes)

    template <typename T>
    inline Q_DECL_CONSTEXPR MessageType messageType()
    {
        return qMetaTypeId<T>();
    }
}
Q_DECLARE_OPERATORS_FOR_FLAGS(QtMpi::CommunicationType)

QT_END_NAMESPACE

#endif // QMPI_GLOBAL_H
