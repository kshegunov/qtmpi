/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpimessagereceiver.h"
#include "qmpievent.h"

#include "qmpimessagereceiver_p.h"

QT_BEGIN_NAMESPACE

template class QHash<QtMpi::MessageType, QMpiMessageNotifier *>;

QMpiMessageReceiver::QMpiMessageReceiver(QMpiMessageReceiverPrivate & d, QObject * parent)
    : QObject(parent), d_ptr(&d)
{
}

QMpiMessageReceiver::~QMpiMessageReceiver()
{
    delete d_ptr;
}

QMpiMessageNotifier * QMpiMessageReceiver::notifier(QtMpi::MessageType typeId)
{
    Q_D(QMpiMessageReceiver);
    QMpiNotifierHash::Iterator iterator = d->notifiers.find(typeId);
    if (iterator == d->notifiers.end())
        iterator = d->notifiers.insert(typeId, new QMpiMessageNotifier(this));

    return iterator.value();
}

bool QMpiMessageReceiver::event(QEvent * e)
{
    if (e->type() == QMpiReceiveEvent::EventType)  {
        QSharedPointer<QMpiMessage> message = reinterpret_cast<QMpiReceiveEvent *>(e)->message();

        // Check if there's someone listening for that message
        Q_D(QMpiMessageReceiver);
        QtMpi::MessageType typeId = message->type();
        if (typeId == QtMpi::UnknownMessageType)
            return false;

        QMpiNotifierHash::Iterator iterator = d->notifiers.find(typeId);
        if (iterator == d->notifiers.end())
            return false;

        emit iterator.value()->received(message);
        return true;
    }

    return QObject::event(e);
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

QMpiMessageReceiverPrivate::~QMpiMessageReceiverPrivate()
{
}

QT_END_NAMESPACE
