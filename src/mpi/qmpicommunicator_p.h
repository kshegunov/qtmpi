/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtMpi API. It exists only
// as an implementation detail. This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#ifndef QMPICOMMUNICATOR_P_H
#define QMPICOMMUNICATOR_P_H

#include "qmpiglobal.h"
#include "qmpiexchange.h"
#include "qmpimessagereceiver_p.h"

#include <QList>
#include <QLinkedList>
#include <QHash>
#include <QPointer>

#include <mpi/mpi.h>

class QMpiNode;
class QMpiRequest;

extern template class QList<QMpiNode *>;
using QMpiNodeList = QList<QMpiNode *>;

extern template class QHash<QtMpi::NodeId, QMpiNode *>;
using QMpiNodeHash = QHash<QtMpi::NodeId, QMpiNode *>;

class QMpiCommunicator;
class Q_MPI_LOCAL QMpiCommunicatorPrivate : public QMpiMessageReceiverPrivate
{
    typedef QLinkedList<QMpiInboundExchange> InboundQueue;
    typedef QHash<quint32, QMpiOutboundExchange> OutboundQueue;

    Q_DECLARE_PUBLIC(QMpiCommunicator)

private:
    QMpiCommunicatorPrivate(MPI_Comm, QMpiCommunicator * q);

    void process();
    bool hasPendingMessages();

    QMpiNode * getNode(QtMpi::NodeId id) const;
    QMpiNodeList getPeers(QtMpi::NodeId id) const;

    void quit();

public:
    static QMpiCommunicator * createWorld(QObject *);

private:
    QtMpi::NodeId currentNodeId;
    QMpiNodeHash peers;
    MPI_Comm handle;

    InboundQueue inbound;
    OutboundQueue outbound;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QMpiCommunicator * QMpiCommunicatorPrivate::createWorld(QObject * parent)
{
    return new QMpiCommunicator(parent);
}

#endif // QMPICOMMUNICATOR_P_H
