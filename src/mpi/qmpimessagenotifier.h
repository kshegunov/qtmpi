/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPIMESSAGENOTIFIER_H
#define QMPIMESSAGENOTIFIER_H

#include "qmpiglobal.h"

#include <QObject>
#include <QSharedPointer>

QT_BEGIN_NAMESPACE

class QMpiMessage;
extern template class Q_MPI_EXPORT QSharedPointer<QMpiMessage>;

class Q_MPI_EXPORT QMpiMessageNotifier : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(QMpiMessageNotifier)

public:
    using QObject::QObject;

Q_SIGNALS:
    void received(const QSharedPointer<QMpiMessage> &);
};

QT_END_NAMESPACE

#endif // QMPIMESSAGENOTIFIER_H
