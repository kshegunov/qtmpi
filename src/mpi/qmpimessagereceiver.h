/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPIMESSAGERECEIVER_H
#define QMPIMESSAGERECEIVER_H

#include "qmpimessagenotifier.h"

QT_BEGIN_NAMESPACE

class QMpiMessageReceiverPrivate;
class Q_MPI_EXPORT QMpiMessageReceiver : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(QMpiMessageReceiver)
    Q_DECLARE_PRIVATE(QMpiMessageReceiver)

public:
    QMpiMessageReceiver(QMpiMessageReceiverPrivate &, QObject * = nullptr);
    ~QMpiMessageReceiver() override;

public:
    // Bind to pointer-to-members
    template <class MessageType, typename Slot>
    typename QMetaObject::Connection bind(const typename QtPrivate::FunctionPointer<Slot>::Object * receiver, Slot slot, Qt::ConnectionType type = Qt::AutoConnection);

    // Bind to static/global functions with a context object
    template <class MessageType, typename Functor>
    typename std::enable_if<QtPrivate::FunctionPointer<Functor>::ArgumentCount >= 0 && !QtPrivate::FunctionPointer<Functor>::IsPointerToMemberFunction, QMetaObject::Connection>::type bind(const QObject * context, Functor slot, Qt::ConnectionType type = Qt::AutoConnection);

    // Bind to functors (lambdas included) with a context object
    template <class MessageType, typename Functor>
    typename std::enable_if<QtPrivate::FunctionPointer<Functor>::ArgumentCount == -1, QMetaObject::Connection>::type bind(const QObject * context, Functor slot, Qt::ConnectionType type = Qt::AutoConnection);

    // Bind to functors or functions (no context object)
    template <class MessageType, typename Functor>
    typename QMetaObject::Connection bind(Functor slot);

protected:
    QMpiMessageNotifier * notifier(QtMpi::MessageType);
    bool event(QEvent *) override;

protected:
    QMpiMessageReceiverPrivate * d_ptr;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

// Bind to pointer-to-members
template <class MessageType, typename Slot>
inline typename QMetaObject::Connection QMpiMessageReceiver::bind(const typename QtPrivate::FunctionPointer<Slot>::Object * receiver, Slot slot, Qt::ConnectionType type)
{
    typedef QtPrivate::FunctionPointer<decltype(&QMpiMessageNotifier::received)> SignalType;
    typedef QtPrivate::FunctionPointer<Slot> SlotType;

    typedef QtPrivate::List<const MessageType &> SignalArguments;

    Q_STATIC_ASSERT_X((std::is_base_of<QMpiMessage, MessageType>::value), "All MPI message classes must derive from QMpiMessage");
    // Check the compatibility of arguments (from Qt's source).
    Q_STATIC_ASSERT_X(int(SignalType::ArgumentCount) >= int(SlotType::ArgumentCount), "The slot requires more arguments than the signal provides.");
    Q_STATIC_ASSERT_X((QtPrivate::CheckCompatibleArguments<SignalArguments, typename SlotType::Arguments>::value), "Signal and slot arguments are not compatible.");
    Q_STATIC_ASSERT_X((QtPrivate::AreArgumentsCompatible<typename SlotType::ReturnType, SignalType::ReturnType>::value), "Return type of the slot is not compatible with the return type of the signal.");

    return QObject::connect(notifier(QtMpi::messageType<MessageType>()), &QMpiMessageNotifier::received, receiver, [slot, receiver] (const QSharedPointer<QMpiMessage> & message) -> void {
        void * arguments[2] = { nullptr, message.data() };
        SlotType::template call<typename SlotType::Arguments, typename SignalType::ReturnType>(slot, const_cast<typename SlotType::Object *>(receiver), arguments);
    } , type);
}

// Bind to static/global functions with a context object
template <class MessageType, typename Functor>
inline typename std::enable_if<QtPrivate::FunctionPointer<Functor>::ArgumentCount >= 0 && !QtPrivate::FunctionPointer<Functor>::IsPointerToMemberFunction, QMetaObject::Connection>::type QMpiMessageReceiver::bind(const QObject * context, Functor slot, Qt::ConnectionType type)
{
    typedef QtPrivate::FunctionPointer<decltype(&QMpiMessageNotifier::received)> SignalType;
    typedef QtPrivate::FunctionPointer<Functor> SlotType;

    typedef QtPrivate::List<const MessageType &> SignalArguments;

    Q_STATIC_ASSERT_X((std::is_base_of<QMpiMessage, MessageType>::value), "All MPI message classes must derive from QMpiMessage");
    // Check the compatibility of arguments (from Qt's source).
    Q_STATIC_ASSERT_X(int(SignalType::ArgumentCount) >= int(SlotType::ArgumentCount), "The slot requires more arguments than the signal provides.");
    Q_STATIC_ASSERT_X((QtPrivate::CheckCompatibleArguments<SignalArguments, typename SlotType::Arguments>::value), "Signal and slot arguments are not compatible.");
    Q_STATIC_ASSERT_X((QtPrivate::AreArgumentsCompatible<typename SlotType::ReturnType, SignalType::ReturnType>::value), "Return type of the slot is not compatible with the return type of the signal.");

    return QObject::connect(notifier(QtMpi::messageType<MessageType>()), &QMpiMessageNotifier::received, context, [slot] (const QSharedPointer<QMpiMessage> & message) -> void {
        void * arguments[2] = { nullptr, message.data() };
        SlotType::template call<typename SlotType::Arguments, typename SignalType::ReturnType>(slot, nullptr, arguments);
    }, type);
}

// Bind to functors (lambdas included) with a context object
template <class MessageType, typename Functor>
inline typename std::enable_if<QtPrivate::FunctionPointer<Functor>::ArgumentCount == -1, QMetaObject::Connection>::type QMpiMessageReceiver::bind(const QObject * context, Functor slot, Qt::ConnectionType type)
{
    typedef QtPrivate::List<const MessageType &> SignalArguments;

    Q_STATIC_ASSERT_X((std::is_base_of<QMpiMessage, MessageType>::value), "All MPI message classes must derive from QMpiMessage");
    // Check the compatibility of arguments (from Qt's source).
    Q_CONSTEXPR int FunctorArgumentCount = QtPrivate::ComputeFunctorArgumentCount<Functor, SignalArguments>::Value;
    Q_STATIC_ASSERT_X((FunctorArgumentCount >= 0), "Signal and slot arguments are not compatible.");
    Q_CONSTEXPR int SlotArgumentCount = (FunctorArgumentCount >= 0) ? FunctorArgumentCount : 0;

    auto functor = new QtPrivate::QFunctorSlotObject<Functor, SlotArgumentCount, typename QtPrivate::List_Left<SignalArguments, SlotArgumentCount>::Value, void>(slot);
    return QObject::connect(notifier(QtMpi::messageType<MessageType>()), &QMpiMessageNotifier::received, context, [functor] (const QSharedPointer<QMpiMessage> & message) -> void {
        void * arguments[2] = { nullptr, message.data() };
        functor->call(nullptr, arguments);
    }, type);
}

// Bind to functors or functions (no context object)
template <class MessageType, typename Functor>
inline typename QMetaObject::Connection QMpiMessageReceiver::bind(Functor slot)
{
    return bind<MessageType, Functor>(this, slot, Qt::DirectConnection);
}

QT_END_NAMESPACE

#endif // QMPIMESSAGERECEIVER_H
