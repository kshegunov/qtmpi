/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpiexchange.h"
#include "qmpimessage.h"
#include "qmpirequest.h"
#include "qmpiengine_p.h"

#include <QDataStream>
#include <QTimer>

QT_BEGIN_NAMESPACE

QMpiInboundExchange::QMpiInboundExchange()
    : exchangeId(0), handle(MPI_REQUEST_NULL), acknowledge(false)
{
}

QMpiMessage * QMpiInboundExchange::message()
{
    if (!QMetaType::isRegistered(messageType()))  {
        qWarning("QtMpi: Received a message of unknown type.");
        return nullptr;
    }

    QDataStream stream(&data, QIODevice::ReadOnly);
    stream >> exchangeId >> acknowledge;

    // Create the message from the metatype
    QMpiMessage * result = reinterpret_cast<QMpiMessage *>(QMetaType::create(status.MPI_TAG));
    stream >> *result;

    if (stream.status() != QDataStream::Ok)  {
        QMetaType::destroy(status.MPI_TAG, result);

        qWarning("QtMpi: Message deserialization failed.");
        return nullptr;
    }

    return result;
}

bool QMpiInboundExchange::receive(MPI_Comm & communicator)
{
    int pending;

    if (Q_UNLIKELY(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, communicator, &pending, &status) != MPI_SUCCESS))  {
        qWarning("QtMpi: Probing for messages failed.");
        return false;
    }

    if (!pending)
        return false;

    // We have a message, issue a speculative (asynchronous) receive
    int bytes;
    if (Q_UNLIKELY(MPI_Get_count(&status, MPI_BYTE, &bytes) != MPI_SUCCESS))  {
        qWarning("QtMpi: Couldn't determine inbound message size.");
        return false;
    }

    data.resize(bytes);
    if (Q_UNLIKELY(MPI_Irecv(data.data(), bytes, MPI_BYTE, status.MPI_SOURCE, status.MPI_TAG, communicator, &handle) != MPI_SUCCESS))
        return false;

    return true;
}

void QMpiInboundExchange::prepare()
{
    QDataStream stream(&data, QIODevice::ReadOnly);
    stream >> exchangeId >> acknowledge;
}

quint32 QMpiOutboundExchange::exchangeIdCounter = 0;        // The highest bit is reserved

QMpiOutboundExchange::QMpiOutboundExchange(QMpiRequest * interface)
    : exchangeId(exchangeIdCounter++), handle(MPI_REQUEST_NULL), request(interface), synchronous(false), acknowledged(false)
{
}

bool QMpiOutboundExchange::send(QtMpi::NodeId node, const QMpiMessage & message, MPI_Comm & communicator, QtMpi::CommunicationType communicationType)
{
    synchronous = communicationType.testFlag(QtMpi::SynchronousCommunication);

    // First serialize the message and data to a regular byte array
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << exchangeId << synchronous << message;

    // Send out the actual message
    int metaType = QMetaType::type(message.metaObject()->className());
    if (metaType == QMetaType::UnknownType)  {
        qWarning("QtMpi: Unknown message type for class %s", message.metaObject()->className());
        return false;
    }

    if (Q_UNLIKELY(MPI_Isend(data.constData(), data.size(), MPI_BYTE, node, metaType, communicator, &handle) != MPI_SUCCESS))
        return false;

    timer.start();
    return true;
}

void QMpiOutboundExchange::cancel()
{
    if ((synchronous && acknowledged) || (!synchronous && handle == MPI_REQUEST_NULL))
        return;

    if (handle != MPI_REQUEST_NULL)  {
        MPI_Cancel(&handle);
        handle = MPI_REQUEST_NULL;
    }

    if (request)
        QTimer::singleShot(0, request, &QMpiRequest::canceled);
}

void QMpiOutboundExchange::timeout()
{
    if (handle != MPI_REQUEST_NULL)  {
        MPI_Cancel(&handle);
        handle = MPI_REQUEST_NULL;
    }

    if (request)
        QTimer::singleShot(0, request, std::bind(&QMpiRequest::error, request, QMpiRequest::TimeoutError));
}

void QMpiOutboundExchange::notify()
{
    if (request)
        QTimer::singleShot(0, request, &QMpiRequest::finished);
}

QT_END_NAMESPACE
