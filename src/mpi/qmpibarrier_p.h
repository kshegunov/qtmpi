/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtMpi API. It exists only
// as an implementation detail. This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#ifndef QMPIBARRIER_P_H
#define QMPIBARRIER_P_H

#include "qmpiglobal.h"
#include "qmpicompoundrequest.h"

#include <QEventLoop>
#include <QTimer>

QT_BEGIN_NAMESPACE

class QMpiRequest;

class QMpiBarrier;
class Q_MPI_LOCAL QMpiBarrierPrivate
{
    Q_DECLARE_PUBLIC(QMpiBarrier)

public:
    explicit QMpiBarrierPrivate(QMpiBarrier * q);

private:
    void eventLoopTimeout();

private:
    QMpiBarrier * q_ptr;
    QMpiCompoundRequest request;
    QEventLoop eventLoop;
    QTimer timeoutTimer;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QMpiBarrierPrivate::QMpiBarrierPrivate(QMpiBarrier * q)
    : q_ptr(q), eventLoop(q), timeoutTimer(q)
{
    QObject::connect(q, &QMpiBarrier::finished, &eventLoop, &QEventLoop::quit);
    QObject::connect(&timeoutTimer, &QTimer::timeout, q, std::bind(&QMpiBarrierPrivate::eventLoopTimeout, this));

    timeoutTimer.setSingleShot(true);
}

inline void QMpiBarrierPrivate::eventLoopTimeout()
{
    eventLoop.exit(1);
}

QT_END_NAMESPACE

#endif // QMPIBARRIER_P_H
