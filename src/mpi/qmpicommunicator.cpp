/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpicommunicator.h"
#include "qmpicommunicator_p.h"
#include "qmpinode.h"
#include "qmpinode_p.h"
#include "qmpiengine_p.h"
#include "qmpirequest.h"
#include "qmpicompoundrequest.h"
#include "qmpimessage.h"
#include "qmpimessage_p.h"
#include "qmpiexchange.h"
#include "qmpievent.h"

#include <QCoreApplication>

QT_BEGIN_NAMESPACE

template class QHash<QtMpi::NodeId, QMpiNode *>;

/*!
    \class QMpiCommunicator
    \inmodule QtMpi

    \brief The \l{QMpiCommunicator} class represents a communicator in the MPI infrastructure.

    A communicator is a collection of processes. There is one and only one global (world)
    communicator but others can be created to split up the original set of processes
    into smaller groups.

    \sa QMpiNode
*/

/*!
    \fn bool QMpiCommunicator::isValid() const

    Returns \c true if the communicator is valid; otherwise returns \c false.
*/

/*!
    \internal

    Creates the default (world) communicator.
 */
QMpiCommunicator::QMpiCommunicator(QObject * parent)
    : QMpiCommunicator(*new QMpiCommunicatorPrivate(MPI_COMM_WORLD, this), parent)
{
}

/*!
    \internal

    Creates a communicator from the private object \a d and the QObject \a parent.
*/
QMpiCommunicator::QMpiCommunicator(QMpiCommunicatorPrivate & d, QObject * parent)
    : QMpiMessageReceiver(d, parent)
{
    // Get the current node id
    if (MPI_Comm_rank(d.handle, &d.currentNodeId) != MPI_SUCCESS)  {
        qWarning("QtMpi: Unable to determine current process' rank.");
        return;
    }

    // Set the expected number of active nodes
    int numberOfPeers;
    if (Q_UNLIKELY(MPI_Comm_size(d.handle, &numberOfPeers) != MPI_SUCCESS))  {
        qWarning("QtMpi[%d]: Unable to determine the number of nodes.", d.currentNodeId);
        return;
    }

    // Create the nodes
    d.peers.reserve(numberOfPeers);
    for (qint32 id = 0; id < numberOfPeers; id++)  {
        QMpiNode * node = QMpiNodePrivate::create(id, this);
        d.peers.insert(id, node);

        QObject::connect(node, &QMpiNode::aboutToQuit, this, [node] () -> void { QMpiNodePrivate::deactivate(node); });
        if (id == d.currentNodeId)
            QObject::connect(qApp, &QCoreApplication::aboutToQuit, node, &QMpiNode::aboutToQuit);
    }

    // Connect the poll timer to the processing routine for this communicator
    QObject::connect(QMpiEnginePrivate::instance()->timer(), &QTimer::timeout, this, std::bind(&QMpiCommunicatorPrivate::process, &d));
    // Connect the about to quit signal from the application to the clean up routines
    QObject::connect(qApp, &QCoreApplication::aboutToQuit, this, std::bind(&QMpiCommunicatorPrivate::quit, &d));

    QTimer::singleShot(0, this, &QMpiCommunicator::opened);
}

/*!
    Destroys the communicator and frees the associated resources.
*/
QMpiCommunicator::~QMpiCommunicator()
{
    delete d_ptr;
}

/*!
    Returns the global (world) communicator.
*/
QMpiCommunicator * QMpiCommunicator::world()
{
    return QMpiEnginePrivate::instance()->communicator();
}

/*!
    Returns the current node.
*/
QMpiNode * QMpiCommunicator::currentNode() const
{
    Q_D(const QMpiCommunicator);
    return d->getNode(d->currentNodeId);
}

/*!
    Returns the root node.
    \note The root node is the process with identifier \c 0.
*/
QMpiNode * QMpiCommunicator::rootNode() const
{
    Q_D(const QMpiCommunicator);
    return d->getNode(QtMpi::RootNodeId);
}

/*!
    Returns the list of all nodes in the communicator.
*/
QMpiNodeList QMpiCommunicator::peers() const
{
    Q_D(const QMpiCommunicator);
    return d->getPeers(d->currentNodeId);
}

/*!
    Returns the peers for the \a node.
*/
QMpiNodeList QMpiCommunicator::peers(const QMpiNode * node) const
{
    Q_D(const QMpiCommunicator);
    return d->getPeers(node->id());
}

/*!
    Returns the current node's identifier.
*/
QtMpi::NodeId QMpiCommunicator::currentNodeId() const
{
    Q_D(const QMpiCommunicator);
    return d->currentNodeId;
}

/*!
    Returns the number of processes in the communicator.
*/
qint32 QMpiCommunicator::size() const
{
    Q_D(const QMpiCommunicator);
    return d->peers.size();
}

/*!
    Sends the \a message to the \a node in the communicator using the specified \a communicationType.
    Returns a request object that can be used to monitor the progress of the operation.

    \sa QMpiNode, QMpiMessage, QMpiRequest, QtMpi::CommunicationType
*/
QMpiRequest * QMpiCommunicator::send(QMpiNode * node, const QMpiMessage & message, QtMpi::CommunicationType communicationType)
{
    Q_D(QMpiCommunicator);
    QMpiRequest * request = new QMpiRequest(this);

    if (node->id() == d->currentNodeId)  {
        // Enable loopback: Copy the message to be sent & complete the request
        QSharedPointer<QMpiMessage> loopbackMessage(reinterpret_cast<QMpiMessage *>(QMetaType::create(message.type(), &message)));
        QTimer::singleShot(0, request, &QMpiRequest::finished); // Loop back always succeeds

        // Post the receive event to the communicator and to the node
        QCoreApplication::postEvent(this, new QMpiReceiveEvent(loopbackMessage));
        QCoreApplication::postEvent(node, new QMpiReceiveEvent(loopbackMessage));
    }
    else  {
        QMpiOutboundExchange exchange(request);
        if (!exchange.send(node->id(), message, d->handle, communicationType))
            QTimer::singleShot(0, request, std::bind(&QMpiRequest::error, request, QMpiRequest::SendingFailedError));
        else
            d->outbound.insert(exchange.id(), exchange);
    }

    return request;
}

/*!
    Sends the \a message to all the nodes in the communicator excluding the current one using the specified \a communicationType.
    Returns a request object that can be used to monitor the progress of the operation.

    \sa QMpiNode, QMpiMessage, QMpiRequest, QtMpi::CommunicationType
*/
QMpiRequest * QMpiCommunicator::broadcast(const QMpiMessage & message, QtMpi::CommunicationType communicationType)
{
    QMpiCompoundRequest * result = new QMpiCompoundRequest(this);

    QMpiNodeList nodes = peers();
    if (nodes.size() == 0)  {
        QTimer::singleShot(0, result, &QMpiRequest::finished);
        return result;
    }

    for (QMpiNode * node : nodes)
        result->watch(send(node, message, communicationType));

    return result;
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

QMpiCommunicatorPrivate::QMpiCommunicatorPrivate(MPI_Comm communicatorHandle, QMpiCommunicator * q)
    : QMpiMessageReceiverPrivate(q), currentNodeId(0), handle(communicatorHandle)
{
}

void QMpiCommunicatorPrivate::process()
{
    Q_Q(QMpiCommunicator);
    // Poll the MPI messages
    while (hasPendingMessages())  {
        QMpiInboundExchange exchange;
        if (!exchange.receive(handle))  {
            qWarning("QtMpi: Error while receiving an MPI message");
            break;
        }

        inbound.append(exchange);
    }

    // See what's the status of our inbound messages and activate the respective signals if needed
    for (InboundQueue::Iterator exchange = inbound.begin(); exchange != inbound.end(); )  {
        if (exchange->isActive())  {
            ++exchange;
            continue;
        }

        QMpiNode * node = getNode(exchange->nodeId());

        // If the sender expects acknowledgement send it back
        if (exchange->isSynchronous())
            q->send(node, QMpiAcknowledgeMessage(exchange->requestId()));

        QSharedPointer<QMpiMessage> message(exchange->message());
        exchange = inbound.erase(exchange);

        if (!message)
            continue;

        QtMpi::MessageType messageType = message->type();
        if (messageType == qMetaTypeId<QMpiAcknowledgeMessage>())  {
            // If it's an acknowledgement from someone handle it immediately
            quint32 exchangeId = reinterpret_cast<QMpiAcknowledgeMessage *>(message.data())->id();

            OutboundQueue::Iterator exchange = outbound.find(exchangeId);
            if (exchange == outbound.end())  {
                qWarning("QtMpi: Received acknowledgement for an unexisting request #%u", exchangeId);
                continue;
            }

            exchange->acknowledge();
            continue;
        }
        else if (messageType == qMetaTypeId<QMpiQuitMessage>())  {
            QCoreApplication::postEvent(getNode(currentNodeId), new QMpiReceiveEvent(message));
            continue;
        }

        // Post the receive event to the communicator and to the node so attached handlers are triggered after we return control to the event loop
        QCoreApplication::postEvent(q, new QMpiReceiveEvent(message));
        QCoreApplication::postEvent(node, new QMpiReceiveEvent(message));
    }

    // See what's the status of our outgoing requests
    qint64 timeout = QMpiEnginePrivate::instance()->maxLatency();
    for (OutboundQueue::Iterator exchange = outbound.begin(); exchange != outbound.end(); )  {
        if (exchange->isActive())  {
            if (!exchange->isStale(timeout))  {
                ++exchange;
                continue;
            }

            qWarning("QtMpi: A request has gone stale. It will be discarded.");
            exchange->timeout();
        }
        else
            exchange->notify();

        exchange = outbound.erase(exchange);
    }
}

inline bool QMpiCommunicatorPrivate::hasPendingMessages()
{
    int flag;
    if (Q_UNLIKELY(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, handle, &flag, MPI_STATUS_IGNORE) != MPI_SUCCESS))
        return false;

    return flag;
}

inline QMpiNode * QMpiCommunicatorPrivate::getNode(QtMpi::NodeId id) const
{
    QMpiNodeHash::ConstIterator iterator = peers.find(id);
    return iterator != peers.end() ? iterator.value() : nullptr;
}

inline QMpiNodeList QMpiCommunicatorPrivate::getPeers(QtMpi::NodeId id) const
{
    QMpiNodeList result;
    result.reserve(peers.size() - 1);

    for (QMpiNodeHash::ConstIterator i = peers.constBegin(), end = peers.constEnd(); i != end; ++i)  {
        QMpiNode * node = i.value();
        if (i.key() == id || !node->isActive())
            continue;

        result.append(node);
    }

    return result;
}

inline void QMpiCommunicatorPrivate::quit()
{
    Q_Q(QMpiCommunicator);
    QMpiRequest * request = q->broadcast(QMpiAboutToQuitMessage(), QtMpi::SynchronousCommunication);

    QObject::connect(request, &QMpiRequest::error, request, &QMpiRequest::finished);
    QObject::connect(request, &QMpiRequest::finished, q, &QMpiCommunicator::closed);
}

QT_END_NAMESPACE
