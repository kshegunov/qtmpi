/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpibarrier.h"
#include "qmpibarrier_p.h"
#include "qmpirequest.h"

#include <QVector>

QT_BEGIN_NAMESPACE

/*!
    \class QMpiBarrier
    \inmodule QtMpi

    \brief The \l{QMpiBarrier} class provides a means for synchronizing a number of active requests.

    It processes the application command line and provides for
    both the daemon itself and the controlling application.
    The application behavior is controlled through command line switches
    passed on startup. When no switches are provided the command line help text
    is displayed. The application object will emit the appropriate signal
    associated with a switch when there are no errors processing the command line.

    \sa QMpiRequest
*/
/*!
    \fn void QMpiBarrier::finished()

    This signal is emitted when all the requests currently observed by the barrier
    have completed.
*/
/*!
    \fn void QMpiBarrier::canceled(QMpiRequest * request)

    This signal is emitted when the observed request \a request has been canceled.
*/
/*!
    \fn void QMpiBarrier::error(QMpiRequest * request)

    This signal is emitted when the observed request \a request has signaled an error.
*/

/*!
    Constructs the barrier object with parent object \a parent.
*/
QMpiBarrier::QMpiBarrier(QObject * parent)
    : QObject(parent), d_ptr(new QMpiBarrierPrivate(this))
{
}

/*!
    Destroys the barrier object.
*/
QMpiBarrier::~QMpiBarrier()
{
    delete d_ptr;
}

/*!
    Registers the request \a request to be observed by the barrier.
*/
void QMpiBarrier::addRequest(QMpiRequest * request)
{
    Q_D(QMpiBarrier);
    d->request.watch(request);
}

/*!
    Deregisters the request \a request from the barrier.
*/
void QMpiBarrier::removeRequest(QMpiRequest * request)
{
    Q_D(QMpiBarrier);
    d->request.forget(request);
}

/*!
    Blocks until all the observed requests have finished and returns \c true.
    Conversly, if the timeout \a timeout in milliseconds has expired, any of the observed requests is canceled or emits an error, it returns \c false.

    \warning This method uses a local event loop.
 */
bool QMpiBarrier::wait(int timeout)
{
    Q_D(QMpiBarrier);
    if (!d->request.isWatching())
        return true;    // Nothing to do

    if (timeout != WaitForever)
        d->timeoutTimer.start(timeout);

    QMetaObject::Connection connection = QObject::connect(&d->request, &QMpiRequest::finished, this, &QMpiBarrier::finished);
    bool status = !d->eventLoop.exec(); // Any nonzero value means it timed out
    QObject::disconnect(connection);

    return status;
}

/*!
    Blocks until any of the observed requests have finished and returns \c true.
    Conversly, if the timeout \a timeout in milliseconds has expired, any of the observed requests are canceled or emit an error, it returns \c false.

    \warning This method uses a local event loop.
 */
bool QMpiBarrier::waitAny(int timeout)
{
    Q_D(QMpiBarrier);
    if (!d->request.isWatching())
        return true;    // Nothing to do

    if (timeout != WaitForever)
        d->timeoutTimer.start(timeout);

    QMetaObject::Connection connection = QObject::connect(&d->request, &QMpiCompoundRequest::finished, this, &QMpiBarrier::finished);
    bool status = !d->eventLoop.exec(); // Any nonzero value means it timed out
    QObject::disconnect(connection);

    return status; // Any nonzero value means it timed out
}

QT_END_NAMESPACE
