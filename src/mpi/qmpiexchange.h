/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtMpi API. It exists only
// as an implementation detail. This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#ifndef QMPIEXCHANGE_H
#define QMPIEXCHANGE_H

#include "qmpiglobal.h"

#include <QByteArray>
#include <QPointer>
#include <QElapsedTimer>

#include <mpi/mpi.h>

QT_BEGIN_NAMESPACE

class QMpiRequest;
class QMpiMessage;

class Q_MPI_LOCAL QMpiInboundExchange
{
public:
    QMpiInboundExchange();

    QtMpi::NodeId nodeId() const;
    QtMpi::MessageType messageType() const;
    QMpiMessage * message();

    bool isSynchronous() const;
    quint32 requestId() const;

    bool receive(MPI_Comm &);
    bool isActive();
    void prepare();

private:
    quint32 exchangeId;
    MPI_Request handle;
    MPI_Status status;
    QByteArray data;
    bool acknowledge;
};

class Q_MPI_LOCAL QMpiOutboundExchange
{
public:
    QMpiOutboundExchange(QMpiRequest *);
    bool send(QtMpi::NodeId, const QMpiMessage &, MPI_Comm &, QtMpi::CommunicationType);

    quint32 id() const;

    void acknowledge();
    bool isActive();
    bool isStale(qint64);
    void timeout();
    void cancel();
    void notify();

private:
    quint32 exchangeId;
    MPI_Request handle;
    QByteArray data;
    QPointer<QMpiRequest> request;
    bool synchronous, acknowledged;
    QElapsedTimer timer;

    static quint32 exchangeIdCounter;
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QtMpi::NodeId QMpiInboundExchange::nodeId() const
{
    return status.MPI_SOURCE;
}

inline QtMpi::MessageType QMpiInboundExchange::messageType() const
{
    return status.MPI_TAG;
}

inline bool QMpiInboundExchange::isSynchronous() const
{
    return acknowledge;
}

inline quint32 QMpiInboundExchange::requestId() const
{
    return exchangeId;
}

inline bool QMpiInboundExchange::isActive()
{
    int completed;
    if (handle == MPI_REQUEST_NULL || MPI_Test(&handle, &completed, MPI_STATUS_IGNORE) != MPI_SUCCESS)
        return false;

    if (completed)
        prepare();

    return !completed;
}

inline quint32 QMpiOutboundExchange::id() const
{
    return exchangeId;
}

inline void QMpiOutboundExchange::acknowledge()
{
    acknowledged = true;
}

inline bool QMpiOutboundExchange::isActive()
{
    if (synchronous)
        return !acknowledged;

    int completed = 1;
    if (handle == MPI_REQUEST_NULL || MPI_Test(&handle, &completed, MPI_STATUS_IGNORE) != MPI_SUCCESS)
        return false;

    return !completed;
}

inline bool QMpiOutboundExchange::isStale(qint64 timeout)
{
    return timer.hasExpired(timeout);
}

QT_END_NAMESPACE

#endif // QMPIEXCHANGE_H
