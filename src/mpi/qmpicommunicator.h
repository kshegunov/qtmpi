/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPICOMMUNICATOR_H
#define QMPICOMMUNICATOR_H

#include "qmpiglobal.h"
#include "qmpimessagereceiver.h"

QT_BEGIN_NAMESPACE

class QMpiNode;
class QMpiRequest;
class QMpiMessage;
class QMpiQuitMessage;

extern template class QList<QMpiNode *>;
using QMpiNodeList = QList<QMpiNode *>;

class QMpiCommunicatorPrivate;
class Q_MPI_EXPORT QMpiCommunicator : public QMpiMessageReceiver
{
    Q_OBJECT
    Q_DISABLE_COPY(QMpiCommunicator)
    Q_DECLARE_PRIVATE(QMpiCommunicator)

private:
    QMpiCommunicator(QObject * = nullptr);

public:
    explicit QMpiCommunicator(QMpiCommunicatorPrivate &, QObject * = nullptr);
    ~QMpiCommunicator() override;

    bool isValid() const;

    static QMpiCommunicator * world();

    QMpiNode * currentNode() const;
    QMpiNode * rootNode() const;
    QMpiNodeList peers() const;
    QMpiNodeList peers(const QMpiNode *) const;

    QtMpi::NodeId currentNodeId() const;
    qint32 size() const;

    QMpiRequest * send(QMpiNode *, const QMpiMessage &, QtMpi::CommunicationType = QtMpi::AutoCommunication);
    QMpiRequest * broadcast(const QMpiMessage &, QtMpi::CommunicationType = QtMpi::AutoCommunication);

Q_SIGNALS:
    void opened();
    void closed();
};

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline bool QMpiCommunicator::isValid() const
{
    return size();
}

QT_END_NAMESPACE

#endif // QMPICOMMUNICATOR_H
