/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#include "qmpiengine.h"
#include "qmpiengine_p.h"
#include "qmpicommunicator.h"
#include "qmpicommunicator_p.h"
#include "qmpimessage_p.h"
#include "qmpilog.h"

#include <QCoreApplication>
#include <QTimer>
#include <QEventLoop>

QT_BEGIN_NAMESPACE

/*!
    \class QMpiEngine
    \inmodule QtMpi

    \brief The \l{QMpiEngine} class provides the intialziation and clean up for the MPI infrastructure.
    It also provides means to control the MPI infrastructure's runtime parameters as timouts and latency.

    \sa QMpiCommunicator
*/

/*!
    Constructs the MPI engine object with parent object \a parent.
    Initializes the MPI infrastructure and initiates the message reception.

    The \a argc and \a argv arguments are processed by the underlying MPI implementation and may be modified.

    \note \l{QMpiEngine} must be instantiated after the \l{QCoreApplication} has been created.
    \note There could exist only one \l{QMpiEngine} object.
*/
QMpiEngine::QMpiEngine(int & argc, char ** & argv, QObject * parent)
    : QObject(parent), d_ptr(new QMpiEnginePrivate(argc, argv, this))
{
    Q_ASSERT(qApp);

//    qRegisterMetaType<QMpiAcknowledgeMessage>();
//    qRegisterMetaType<QMpiQuitMessage>();
//    qRegisterMetaType<QMpiAboutToQuitMessage>();

    // Start the engine
    QMetaObject::invokeMethod(this, std::bind(&QMpiEnginePrivate::start, d_ptr));
}

/*!
    Destroys the engine object.
    Performs the clean up of resources and deregisters the process from the MPI infrastructure.
*/
QMpiEngine::~QMpiEngine()
{
    delete d_ptr;
}

/*!
    Sets the message reception timeout to \a timeout milliseconds
*/
void QMpiEngine::setTimeout(qint64 timeout)
{
    Q_D(QMpiEngine);
    d->timeout = timeout;
}

/*!
    Returns the currently set message reception timeout.
*/
qint64 QMpiEngine::timeout() const
{
    Q_D(const QMpiEngine);
    return d->timeout;
}

/*!
    Sets the message poll interval in milliseconds.
*/
void QMpiEngine::setPollInterval(int interval)
{
    Q_D(QMpiEngine);
    d->pollTimer.setInterval(interval);
}

/*!
    Returns the message poll interval.
*/
int QMpiEngine::pollInterval() const
{
    Q_D(const QMpiEngine);
    return d->pollTimer.interval();
}

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

QMpiEnginePrivate * QMpiEnginePrivate::engineInstance = nullptr;

QMpiEnginePrivate::QMpiEnginePrivate(int & argc, char ** & argv, QMpiEngine * q)
    : q_ptr(q), timeout(QMpiEngine::DefaultTimeout), pollTimer(q), state(InactiveState)
{
    Q_ASSERT(!engineInstance);
    engineInstance = this;

    pollTimer.setInterval(QMpiEngine::DefaultPollInterval);

    // Initialize OpenMPI
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS)  {
        qCWarning(Mpi) << "Couldn't initialize OpenMPI.";
        state = InvalidState;
        return;
    }

    globalCommunicator = QMpiCommunicatorPrivate::createWorld(q);
    if (!globalCommunicator->isValid())  {
        qCWarning(Mpi) << "Couldn't initialize the global communicator.";
        state = InvalidState;
        return;
    }

    // Connect the clean up
    QObject::connect(qApp, &QCoreApplication::aboutToQuit, q, [this] () -> void  {
        state = ShuttingDownState;
        shutdown();
    });

    QObject::connect(globalCommunicator, &QMpiCommunicator::opened, q, &QMpiEngine::ready);
    QObject::connect(globalCommunicator, &QMpiCommunicator::closed, q, [this] () -> void  {
        state = InactiveState;
        pollTimer.stop();
    });
}

QMpiEnginePrivate::~QMpiEnginePrivate()
{
    shutdown();

    int initialized = 0;
    MPI_Initialized(&initialized);

    if (initialized)
        MPI_Finalize();
}

void QMpiEnginePrivate::start()
{
    if (state == InvalidState)  {
        qCWarning(Mpi) << "The MPI engine couldn't start.";
        qApp->quit();
        return;     // Something went wrong with the MPI initialization
    }

    Q_ASSERT(state == InactiveState);
    if (state != InactiveState)  {
        qCWarning(Mpi) << "Trying to start the MPI engine but it's already running.";
        return;
    }

    // Start the MPI message pump
    state = RunningState;
    pollTimer.start();
}

void QMpiEnginePrivate::shutdown()
{
    if (state == InvalidState || state == InactiveState || !globalCommunicator->isValid())
        return;     // Nothing to do, we never initialized or we already quit

    if (state != ShuttingDownState)  {
        qCWarning(Mpi) << "Forcing shutdown sequence!";
        qApp->quit();
        return;
    }

    // Process requests and messages until we are ready to shut down cleanly
    Q_Q(QMpiEngine);

    QEventLoop exitLoop(q);
    QObject::connect(globalCommunicator, &QMpiCommunicator::closed, &exitLoop, &QEventLoop::quit);

    exitLoop.exec();
}

QT_END_NAMESPACE
