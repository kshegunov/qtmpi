INCLUDEPATH += $$PWD

QT = core

# TODO: Fix those - Be careful with compiler compatibility
MPICC = mpicc
MPICXX = mpic++

QMAKE_CC = $$MPICC
QMAKE_CXX = $$MPICXX
QMAKE_LINK = $$MPICXX
QMAKE_LINK_SHLIB = $$MPICXX

SOURCES += \
    $$PWD/qmpiengine.cpp \
    $$PWD/qmpimessage.cpp \
    $$PWD/qmpievent.cpp \
    $$PWD/qmpimessagenotifier.cpp \
    $$PWD/qmpimessagereceiver.cpp \
    $$PWD/qmpinode.cpp \
    $$PWD/qmpicommunicator.cpp \
    $$PWD/qmpiexchange.cpp \
    $$PWD/qmpirequest.cpp \
    $$PWD/qmpicompoundrequest.cpp \
    $$PWD/qmpibarrier.cpp \
    $$PWD/qmpilog.cpp

PUBLIC_HEADERS += \
    $$PWD/qmpiglobal.h \
    $$PWD/qmpiengine.h \
    $$PWD/qmpimessage.h \
    $$PWD/qmpimessagenotifier.h \
    $$PWD/qmpimessagereceiver.h \
    $$PWD/qmpinode.h \
    $$PWD/qmpicommunicator.h \
    $$PWD/qmpirequest.h \
    $$PWD/qmpibarrier.h

PRIVATE_HEADERS += \
    $$PWD/qmpiengine_p.h \
    $$PWD/qmpimessage_p.h \
    $$PWD/qmpievent.h \
    $$PWD/qmpimessagereceiver_p.h \
    $$PWD/qmpinode_p.h \
    $$PWD/qmpicommunicator_p.h \
    $$PWD/qmpiexchange.h \
    $$PWD/qmpirequest_p.h \
    $$PWD/qmpicompoundrequest_p.h \
    $$PWD/qmpicompoundrequest.h \
    $$PWD/qmpibarrier_p.h \
    $$PWD/qmpilog.h

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS
