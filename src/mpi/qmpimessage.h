 /****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPIMESSAGE_H
#define QMPIMESSAGE_H

#include "qmpiglobal.h"

#include <QMetaType>
#include <QDataStream>
#include <QSharedPointer>

#define Q_MPI_MESSAGE \
    Q_GADGET \
    public: \
        virtual const QMetaObject * metaObject() const { return &staticMetaObject; }

QT_BEGIN_NAMESPACE

class Q_MPI_EXPORT QMpiMessage
{
    Q_MPI_MESSAGE

public:
    QMpiMessage();
    virtual ~QMpiMessage();

    friend QDataStream & operator << (QDataStream &, const QMpiMessage &);
    friend QDataStream & operator >> (QDataStream &, QMpiMessage &);

    QtMpi::MessageType type() const;

protected:
    virtual bool serialize(QDataStream &) const;
    virtual bool deserialize(QDataStream &);
};

extern template class QSharedPointer<QMpiMessage>;

// ---------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QDataStream & operator << (QDataStream & stream, const QMpiMessage & message)
{
    if (!message.serialize(stream))
        stream.setStatus(QDataStream::WriteFailed);

    return stream;
}

inline QDataStream & operator >> (QDataStream & stream, QMpiMessage & message)
{
    if (!message.deserialize(stream))
        stream.setStatus(QDataStream::ReadCorruptData);

    return stream;
}

inline QtMpi::MessageType QMpiMessage::type() const
{
    return QMetaType::type(metaObject()->className());
}

QT_END_NAMESPACE

#endif // QMPIMESSAGE_H
