 /****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

#ifndef QMPINODE_H
#define QMPINODE_H

#include "qmpiglobal.h"
#include "qmpimessagereceiver.h"

QT_BEGIN_NAMESPACE

class QMpiNode;
class QMpiRequest;
class QMpiMessage;
class QMpiCommunicator;

extern template class Q_MPI_EXPORT QList<QMpiNode *>;
using QMpiNodeList = QList<QMpiNode *>;

class QMpiNodePrivate;
class Q_MPI_EXPORT QMpiNode : public QMpiMessageReceiver
{
    Q_OBJECT
    Q_DISABLE_COPY(QMpiNode)
    Q_DECLARE_PRIVATE(QMpiNode)

private:
    QMpiNode(QtMpi::NodeId, QMpiCommunicator *);

public:
    QMpiCommunicator * communicator();
    const QMpiCommunicator * communicator() const;

    bool isRoot() const;
    bool isCurrent() const;
    bool isActive() const;

    QtMpi::NodeId id() const;
    QMpiNodeList peers() const;

    QMpiRequest * send(const QMpiMessage &, QtMpi::CommunicationType = QtMpi::AutoCommunication);

Q_SIGNALS:
    void aboutToQuit();

public Q_SLOTS:
    void quit();
};

QT_END_NAMESPACE

#endif // QMPINODE_H
