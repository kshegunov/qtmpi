/****************************************************************************
**
** Copyright (C) 2017 Konstantin Shegunov <kshegunov@gmail.com>
**
** This file is part of the QtMpi library.
**
** The MIT License (MIT)
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is
** furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
** SOFTWARE.
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the QtMpi API. It exists only
// as an implementation detail. This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#ifndef QMPINODE_P_H
#define QMPINODE_P_H

#include "qmpiglobal.h"
#include "qmpimessagereceiver_p.h"

QT_BEGIN_NAMESPACE

class QMpiCommunicator;

class QMpiNode;
class Q_MPI_LOCAL QMpiNodePrivate : public QMpiMessageReceiverPrivate
{
    Q_DECLARE_PUBLIC(QMpiNode)

private:
    QMpiNodePrivate(QtMpi::NodeId, QMpiCommunicator *, QMpiNode *);

public:
    static QMpiNode * create(QtMpi::NodeId, QMpiCommunicator *);
    static void deactivate(QMpiNode *);

private:
    QtMpi::NodeId id;
    QMpiCommunicator * communicator;
    bool active;
};

// --------------------------------------------------------------------------------------------------------------------------------------------------------- //

inline QMpiNodePrivate::QMpiNodePrivate(QtMpi::NodeId nodeId, QMpiCommunicator * comm, QMpiNode * q)
    : QMpiMessageReceiverPrivate(q), id(nodeId), communicator(comm), active(true)
{
}

inline QMpiNode * QMpiNodePrivate::create(QtMpi::NodeId id, QMpiCommunicator * communicator)
{
    return new QMpiNode(id, communicator);
}

inline void QMpiNodePrivate::deactivate(QMpiNode * node)
{
    node->d_func()->active = false;
}

QT_END_NAMESPACE

#endif // QMPINODE_P_H
